#!/usr/bin/python2

class Char_stream:
    def get():
        pass
    def backstep(step=1):
        pass

def concat(str1, str2):
    return str1 + str2

class EOS_exception(Exception):
    pass

class String_stream(Char_stream):
    def __init__(self, string):
        self.string = string
        self.position = 0
    def get(self):
        if self.position == len(self.string):
            raise EOS_exception()
        self.position += 1
        return self.string[self.position-1]
    def backstep(self, step=1):
        self.position -= step
        if self.position < 0:
            raise Nonsense_exception('backstep()-ed too many')

class File_stream(String_stream):
    def __init__(self, path):
        self.f = open(path, 'rb')
        self.string = reduce(concat, self.f.readlines())
        self.position = 0
