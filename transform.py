#!/usr/bin/python2

# Transforms sexps into lisp objects.

from lisp_obj import *
from axioms import *

def _make_atom_from_sexp(sexp):
    if sexp.content[0] == '"' and sexp.content[-1] == '"':
        # is a string
        return Lisp_atom(Lisp_atom_category.string, \
                             sexp.content[1:-1])
    try:
        number = float(sexp.content) # fails if is not number
        return Lisp_atom(Lisp_atom_category.number, \
                             number)
    except ValueError:
        # is a symbol
        return Lisp_atom(Lisp_atom_category.symbol, \
                             sexp.content)

def _make_cons_from_sexp(sexp):
    dot_notation = _make_atom_from_sexp(Sexp(Sexp_type.atom, '.'))
    all_sub_sexps = map(transform_to_lisp_obj, sexp.content)
    result = lisp_nil
    if len(all_sub_sexps) > 2 and is_eq(all_sub_sexps[-2], dot_notation):
        result = all_sub_sexps[-1]
        all_sub_sexps.pop()
        all_sub_sexps.pop()
    for subsexp in reversed(all_sub_sexps):
        if is_eq(subsexp, dot_notation):
            raise Syntax_error('Misused dot')
        result = cons(subsexp, result)
    return result

def transform_to_lisp_obj(sexp):
    if sexp.type == Sexp_type.atom:
        return _make_atom_from_sexp(sexp)
    else:
        return _make_cons_from_sexp(sexp)

def _listify_obj(obj):
    if is_atom(obj):
        return '%d:%s' % (obj.category, obj.content)
    else:
        return [_listify_obj(car(obj)), _listify_obj(cdr(obj))]

def _lisp_obj_to_string(obj):
    if is_atom(obj):
        if obj.category == Lisp_atom_category.string:
            return '"' + obj.content + '"'
        return str(obj.content)
    return '(' + _lisp_obj_to_string(car(obj)) + ' . ' + \
        _lisp_obj_to_string(cdr(obj)) + ')'

def pretty_print_lisp_obj(obj):
    print _lisp_obj_to_string(obj)

# def main():
#     from parse import *
#     path = raw_input('input a file to test transforming: ')
#     stream = File_stream(path)
#     sexp = read_one_sexp(stream)
#     pretty_print_sexp(sexp)
#     obj = transform_to_lisp_obj(sexp)
#     pretty_print_lisp_obj(obj)

# if __name__ == '__main__':
#     main()
