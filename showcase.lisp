;; Basic operation 1 --- suppress evaluation
'x
;; => x
(quote x)
;; same as above
'(1 . 2)
;; => (1.0 . 2.0) --- a cons cell, integers are stored as floats
'(1 2 3)
;; => (1.0 . (2.0 . (3.0 . nil)))
;; --- lists are made up of cons cells, ended by nil

;; Basic operation 2 --- check if object is atom
(atom t)
;; => t
(atom ())
;; => t --- Empty list is nil, an atom too
(atom nil)
;; => t
(atom '(12 34))
;; => nil --- list is not atom

;; Basic operation 3 --- check if object is list
(listp '(1 2 3))
;; => t
(listp nil)
;; => t --- nil, same as empty list, is both atom and list
(listp 12)
;; => nil

;; Basic operation 4 --- check equality
(eq t t)
;; => t
(eq t nil)
;; => nil
(eq '(1 . 2) '(1 . 2))
;; => t
(eq '(1 2 3) '(1 2 3))
;; => t --- test is recursive on a list

;; Basic operation 5 --- make a cons cell
(cons 12 34)
;; => (12.0 . 34.0)
(cons 'a 'b)
;; => (a . b)

;; Basic operation 6 --- car of cons cell
(car (cons 'x 'y))
;; => x
(car '(1 . 2))
;; => 1.0
(car '(1 2 3 4))
;; => 1.0
(car nil)
;; => nil --- a special case, as defined by ANSI Common Lisp

;; Basic operation 7 --- cdr of cons cell
(cdr (cons 'x 'y))
;; => y
(cdr '(23 . 45))
;; => 45
(cdr '(foo bar alice bob))
;; => (bar . (alice . (bob . nil)))
(cdr ())
;; => nil --- another special case, like above

;; Basic operation 8 --- conditional evaluation
(if t 1 2)
;; => 1.0
(if nil 1 2)
;; => 2.0
(if 1 2 3)
;; => 2 --- All non-nils are true

;; Definition facility
(define x 10)
;; Evaluates to nothing, not printed.
x
;; => 10
(define (find-strange-point-in-list cons-cell)
  ;; Checks if a list is well-formed, i.e. made up of
  ;; '(atom . sub-list)
  (if (atom (cdr cons-cell))
      (cdr cons-cell)
      (find-strange-point-in-list (cdr cons-cell))))
(find-strange-point-in-list '(1 2 3 4 5))
;; => nil
(find-strange-point-in-list '(1 2 3 4 . 5))
;; => 5.0

;; Muting variables
(set 'x 15)
;; Also evaluates to nothing.
x
;; => 15.0

;; Basic closure & lexical scoping support
(lambda (x) (car x))
;; => something that looks like a Python object reference
((lambda (x) (car x)) '(13 24))
;; => 13.0
(define (foo)
  (define number 10)
  (lambda () number))
((foo))
;; => 10.0
;; number
;; Not defined in this scope.
;; Uncomment it if you want to see the error message.

;; A more complex closure example, with mutable lexical variable.
(define (setter-and-getter initial-value)
  (define value initial-value)
  (cons (lambda () value)               ; getter
        (lambda (new) (set 'value new)))) ; setter
(define pair (setter-and-getter 10))
(define getter (car pair))
(define setter (cdr pair))
(getter)
;; => 10.0
(setter 20)
(getter)
;; => 20.0
(define another-pair (setter-and-getter 100))
(define another-getter (car another-pair))
(define another-setter (cdr another-pair))
(getter)
;; => 20.0
(another-getter)
;; => 100.0
(another-setter 50)
(another-getter)
;; => 50.0
(getter)
;; => 20.0 --- won't be affected.
