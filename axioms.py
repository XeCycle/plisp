#!/usr/bin/python2

# The most fundamental operations, in Python.

from utils import *
from lisp_obj import *

class Constant_atom(Constant, Lisp_atom):
    pass

# The lisp_nil...
lisp_nil = Constant_atom(Lisp_atom_category.symbol, 'nil')

# and true...
lisp_t = Constant_atom(Lisp_atom_category.symbol, 't')

# Another.
rest_param_symbol = Constant_atom(Lisp_atom_category.symbol, '&rest')

# Here goes the seven axioms.
def quote(obj):
    return obj

# the is_xxx returns True/False, while xxx returns lisp_t/lisp_nil.
def is_atom(obj):
    return obj.type == Lisp_obj_type.atom

def atom(obj):
    if is_atom(obj):
        return lisp_t
    return lisp_nil

def is_eq(obj1, obj2):
    if obj1.type == Lisp_obj_type.atom and obj2.type == Lisp_obj_type.atom:
        return obj1.content == obj2.content
    elif obj1.type == Lisp_obj_type.cons and obj2.type == Lisp_obj_type.cons:
        return is_eq(obj1.car, obj2.car) and \
            is_eq(obj1.cdr, obj2.cdr)
    return False

def eq(obj1, obj2):
    if is_eq(obj1, obj2):
        return lisp_t
    return lisp_nil

def is_list(obj):
    if is_eq(obj, lisp_nil):
        return True
    return obj.type == Lisp_obj_type.cons

def listp(obj):
    if is_list(obj):
        return lisp_t
    return lisp_nil

def car(obj):
    if is_eq(obj, lisp_nil):
        return lisp_nil
    assert obj.type == Lisp_obj_type.cons
    return obj.car

def cdr(obj):
    if is_eq(obj, lisp_nil):
        return lisp_nil
    assert obj.type == Lisp_obj_type.cons
    return obj.cdr

def cons(obj1, obj2):
    return Lisp_cons_cell(obj1, obj2)

def main():
    pass

if __name__ == '__main__':
    main()
