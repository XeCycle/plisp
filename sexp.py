#!/usr/bin/python2

from utils import *

# Classes for representing S-expressions in Py objects.

# Base class.
class Sexp:
    # Constructor, taking obj_type as the type, content as the:
    # literal string for atoms
    # list of Sexp   for lists
    # list of Sexp   for vectors (may not be implemented)
    # flag as the additional flags.
    def __init__(self, obj_type, content):
        self.type = obj_type
        self.content = content

# Some enums
Sexp_type = Enum("atom", "list", "vector")

# Use (quote foo) instead of this.
# Sexp_flags = Enum("normal", "quoted", "half_quoted", "eval_in_half_quote")

def _listify_sexp(sexp):
    if sexp.type == Sexp_type.atom:
        return sexp.content
    if sexp.type == Sexp_type.list:
        result = []
        for subsexp in sexp.content:
            result.append(_listify_sexp(subsexp))
        return result
        
def pretty_print_sexp(sexp):
    print _listify_sexp(sexp)

def main():
    pass

if __name__ == '__main__':
    main()
