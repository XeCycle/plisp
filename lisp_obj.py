#!/usr/bin/python2

from utils import *
from sexp import *

# representation for lisp objects.

class Lisp_obj:
    # Simply a base class.
    type = None

Lisp_obj_type = Enum('atom', 'cons')

Lisp_atom_category = Enum('symbol', 'number', 'string', 'function', 'macro')

class Lisp_atom(Lisp_obj):
    type = Lisp_obj_type.atom
    def __init__(self, category, content):
        self.content = content
        self.category = category

class Lisp_cons_cell(Lisp_obj):
    type = Lisp_obj_type.cons
    def __init__(self, car, cdr):
        self.car = car
        self.cdr = cdr

class Lisp_function(Lisp_atom):
    def __init__(self, args_def, content, env):
        self.category = Lisp_atom_category.function
        self.args_def = args_def
        self.content = content
        self.env = env

class Lisp_macro(Lisp_atom):
    def __init__(self, args_def, content):
        self.category = Lisp_atom_category.macro
        self.args_def = args_def
        self.content = content

def main():
    pass

if __name__ == '__main__':
    main()
