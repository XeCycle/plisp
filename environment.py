#!/usr/bin/python2

# Definition of execution environment.

from lisp_obj import *
from utils import *

class Virtual_table_entry:
    def __init__(self, name, obj):
        self.name = name
        self.obj = obj

class Symtable:
    def __init__(self, parent):
        if parent is not None:
            self.table = parent.table
        else:
            self.table = {}
    def add_symbol(self, name, entry):
        # This updates the symbol if already present.
        self.table[name] = entry
    def del_symbol(self, name):
        if name in self.table:
            self.table.pop(name)
        else:
            raise Execution_error('No symbol '+name+' to delete')
    def find_symbol(self, name):
        if name in self.table:
            return self.table[name]
        raise Execution_error('Symbol '+name+' not found')
    def fork(self):
        # Gets independent to parent table.
        self.table = dict(self.table)

def main():
    pass

if __name__ == '__main__':
    main()
