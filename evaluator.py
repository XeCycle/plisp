#!/usr/bin/python2

# Common evaluators.

from environment import *
from axioms import *
from utils import *
from lisp_obj import *

def eval_lisp_object(obj, env):
    if is_atom(obj):
        if obj.category == Lisp_atom_category.symbol:
            return env.find_symbol(obj.content).obj
        else:
            return obj
    else:
        action = eval_lisp_object(car(obj), env)
        if action.type == Lisp_obj_type.cons \
                or action.category not in (Lisp_atom_category.macro,
                                           Lisp_atom_category.function):
            raise Execution_error('Not a function or macro')
        if action.category == Lisp_atom_category.macro:
            return eval_macro(action, cdr(obj), Symtable(env))
        else:
            return eval_function(action, eval_body_to_list(cdr(obj), env))

def eval_macro(macro, args, env):
    if callable(macro.content):
        # Builtin macro.
        return macro.content(args, env)
    env.fork()
    setup_arg_by_list(macro.args_def, args, env)
    return eval_body_return_last(macro.content, env)

def eval_function(function, args):
    if callable(function.content):
        # Builtin function.
        return function.content(args)
    env = Symtable(function.env)
    env.fork()
    setup_arg_by_list(function.args_def, args, env)
    return eval_body_return_last(function.content, env)

def eval_body_to_list(body, env):
    if is_eq(body, lisp_nil):
        return lisp_nil
    return cons(eval_lisp_object(car(body), env),
                eval_body_to_list(cdr(body), env))

def eval_body_return_last(body, env):
    result = None
    if is_eq(body, lisp_nil):
        raise Execution_error('Evaluating empty body')
    while not is_eq(body, lisp_nil):
        result = eval_lisp_object(car(body), env)
        body = cdr(body)
    return result

def setup_arg_by_list(args_def, args, env):
    while not (is_eq(args_def, lisp_nil) or
               is_eq(args, lisp_nil)):
        name = car(args_def)
        if name.type != Lisp_obj_type.atom or \
                name.category != Lisp_atom_category.symbol:
            raise Syntax_error('Invalid argument definition')
        if is_eq(name, rest_param_symbol):
            name = car(cdr(args_def)).content
            value = args
            env.add_symbol(name, Virtual_table_entry(name, value))
            return
        name = name.content
        value = car(args)
        env.add_symbol(name, Virtual_table_entry(name, value))
        args_def = cdr(args_def)
        args = cdr(args)
    # Checks invalid args(defs).
    if not (is_eq(args_def, lisp_nil) and
            is_eq(args, lisp_nil)):
        raise Syntax_error('Argument number mismatch')

# This is obviously easier to implement.
def if_else(condition, if_clause, else_clause, env):
    if is_eq(eval_lisp_object(condition, env), lisp_nil):
        return eval_lisp_object(else_clause, env)
    return eval_lisp_object(if_clause, env)
