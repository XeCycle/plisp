#!/usr/bin/python2

# Commonly used utilities.

# enum class creator
class Enum(object):
    def __init__(self, *names):
        for number, name in enumerate(names):
            setattr(self, name, number)

# When the program is fucked, raise this.
class Nonsense_exception(Exception):
    def __init__(self, message):
        self.message = message

# A very bad error, indeed
class Syntax_error(Exception):
    def __init__(self, message):
        self.message = message

class Unrecognized_char(Syntax_error):
    def __init__(self, char):
        self.message = 'Unrecognized char ' + char

class Execution_error(Exception):
    def __init__(self, message):
        self.message = message

# Turning escape sequences to real chars.
escape_sequence_table = {
    '\\':'\\',
    'n':'\n',
    't':'\t',
    'v':'\v',
    'e':''
    }
def translate_escape_sequence(char):
    try:
        return escape_sequence_table[char]
    except KeyError:
        raise Syntax_error('Unknown escape sequence ' + char)

class Constant:
    def __setattr__(self, name, value):
        if name in self.__dict__:
            raise Nonsense_exception("You're modifying constants!")
        self.__dict__[name] = value

def main():
    pass

if __name__ == '__main__':
    main()
