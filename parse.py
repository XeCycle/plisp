#!/usr/bin/python2

# The dirty context-based sexp parser.

from sexp import *
from utils import *
from stream import *

# interfaces
class Context:
    # Don't need to be initialized, indeed.  Every context has only
    # one instance.
    #
    # Does this char trigger this context?
    def trigger(self, char):
        pass
    # parse whatever after triggered.
    def parse(self, char):
        pass

# The contexts.
class Atom_context(Context):
    pass

class Literal_atom_context(Atom_context):
    def __init__(self):
        self.last_is_escape = False
        self.sequence = ''
        # doesn't include space here, because handled by escape
        self.acceptable_chars = \
            '1234567890-=~!#$%^&*_+' + \
            'qwertyuiop\\QWERTYUIOP|' + \
            'asdfghjklASDFGHJKL' + \
            'zxcvbnm./ZXCVBNM<>?'
    def _add_char(self, char):
        if self.last_is_escape:
            self.sequence += translate_escape_sequence(char)
            self.last_is_escape = False
        elif char == '\\':
            self.last_is_escape = True
        elif char not in self.acceptable_chars:
            raise Unrecognized_char(char)
        else:
            self.sequence += char
    def trigger(self, char):
        if char in self.acceptable_chars:
            self._add_char(char)
            return True
        return False
    def parse(self, stream):
        try:
            while True:
                self._add_char(stream.get())
        except Unrecognized_char:
            pass
        stream.backstep()
        return Sexp(Sexp_type.atom, self.sequence)
    
class Literal_string_context(Literal_atom_context):
    def __init__(self):
        self.last_is_escape = False
        self.sequence = ''
        self.acceptable_chars = \
            '`1234567890-=~!@#$%^&*()_+' + \
            '	qwertyuiop[]\\QWERTYUIOP{}|' + \
            'asdfghjkl;ASDFGHJKL:' + "'" + \
            'zxcvbnm,./ZXCVBNM<>? ' + \
            '\n\v' + \
            '	' + \
            '' + \
            ''
    def trigger(self, char):
        if char == '"':
            self.sequence += '"'
            return True
        return False
    def parse(self, stream):
        try:
            while True:
                self._add_char(stream.get())
        except Unrecognized_char:
            pass
        return Sexp(Sexp_type.atom, self.sequence+'"')

class List_context(Context):
    def __init__(self):
        self.result = []
    def trigger(self, char):
        return char == '('
    def parse(self, stream):
        try:
            while True:
                sexp = read_one_sexp(stream)
                if sexp is not None:
                    self.result.append(sexp)
        except No_context_triggered:
            char = stream.get()
            if char == ')':
                return Sexp(Sexp_type.list, self.result)
            else:
                raise Syntax_error('List unexpectedly terminated by '+char)

class Vector_context(Context):
    pass

halfquote_depth = 0

class Quote_contexts(Context):
    trigger_char = ''
    symbol = ''
    def trigger(self, char):
        return char == self.trigger_char
    def parse(self, stream):
        try:
            sexp = read_one_sexp(stream)
            return Sexp(Sexp_type.list, \
                            [Sexp(Sexp_type.atom, self.symbol), sexp])
        except No_context_triggered:
            raise Syntax_error("Nothing to quote")

class Quoted_context(Quote_contexts):
    trigger_char = "'"
    symbol = 'quote'

class Halfquoted_context(Quote_contexts):
    trigger_char = '`'
    symbol = 'halfquote'

class Eval_in_half_quote_context(Quote_contexts):
    trigger_char = ','
    symbol = 'eval-in-halfquote'

class Space_context(Context):
    def trigger(self, char):
        return char.isspace()
    def parse(self, stream):
        while stream.get().isspace():
            pass
        stream.backstep()

class Comment_context(Context):
    def trigger(self, char):
        return char == ';'
    def parse(self, stream):
        while stream.get() != '\n':
            pass

class No_context_triggered(Exception):
    pass

def read_one_sexp(stream):
    result = None
    while True:
        all_contexts = [Literal_atom_context(),
                        Literal_string_context(),
                        List_context(),
                        Quoted_context(),
                        Halfquoted_context(),
                        Eval_in_half_quote_context(),
                        Space_context(),
                        Comment_context()]
        c = stream.get()
        for context in all_contexts:
            if context.trigger(c):
                result = context.parse(stream)
                if result is None:
                    break
                else:
                    return result
        else:
            stream.backstep()
            raise No_context_triggered()

def main():
    path = raw_input('input a file name to test parsing: ')
    stream = File_stream(path)
    try:
        sexp = read_one_sexp(stream)
    except Syntax_error as error:
        print error.message
    pretty_print_sexp(sexp)

if __name__ == '__main__':
    main()
