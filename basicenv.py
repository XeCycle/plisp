#!/usr/bin/python2

# Setup basic executing environmont.

from environment import *
from utils import *
from axioms import *
from evaluator import *

# The root of everything.
root_env = Symtable(None)

# Add axioms to the table.

# A helpful helper.
def _defvar(table, name, variable):
    table.add_symbol(name, Virtual_table_entry(name, variable))

_defvar(root_env, 'nil', lisp_nil)
_defvar(root_env, 't', lisp_t)
_defvar(root_env, '&rest', rest_param_symbol)

# Define axiom process.
def _def_simple_macro(table, name, macro):
    table.add_symbol(name,
                     Virtual_table_entry(name,
                                         Lisp_macro(lisp_nil, lambda args, env: macro(args))))

def _def_simple_function(table, name, function):
    table.add_symbol(name,
                     Virtual_table_entry(name,
                                         Lisp_function(lisp_nil, function, root_env)))

def _bind_single_arg_to_py(function):
    return lambda cons: function(car(cons))

def _bind_double_arg_to_py(function):
    return lambda cons: function(car(cons), car(cdr(cons)))

def _bind_triple_arg_to_py(function):
    return lambda cons: function(car(cons),
                                 car(cdr(cons)),
                                 car(cdr(cdr(cons))))

_def_simple_macro(root_env, 'quote', _bind_single_arg_to_py(quote))
_def_simple_function(root_env, 'atom', _bind_single_arg_to_py(atom))
_def_simple_function(root_env, 'eq', _bind_double_arg_to_py(eq))
_def_simple_function(root_env, 'listp', _bind_single_arg_to_py(listp))
_def_simple_function(root_env, 'car', _bind_single_arg_to_py(car))
_def_simple_function(root_env, 'cdr', _bind_single_arg_to_py(cdr))
_def_simple_function(root_env, 'cons', _bind_double_arg_to_py(cons))

def _if_bind(body, env):
    condition = car(body)
    then_clause = car(cdr(body))
    else_clause = car(cdr(cdr(body)))
    return if_else(condition, then_clause, else_clause, env)

root_env.add_symbol('if',
                    Virtual_table_entry('if',
                                        Lisp_macro(lisp_nil, _if_bind)))

# Define the definer.
def define(body, env):
    # The macro `define'.
    name = car(body)
    body = cdr(body)
    if is_atom(name):
        env.add_symbol(name.content,
                       Virtual_table_entry(name.content,
                                           eval_body_return_last(body, env)))
    else:
        args_def = cdr(name)
        name = car(name)
        env.add_symbol(name.content,
                       Virtual_table_entry(name.content,
                                           Lisp_function(args_def,
                                                         body, env)))

root_env.add_symbol('define',
                    Virtual_table_entry('define',
                                        Lisp_macro(lisp_nil, define)))

def define_macro(body, env):
    name = car(car(body))
    args_def = cdr(car(body))
    body = cdr(body)
    env.add_symbol(name.content,
                   Virtual_table_entry(name.content,
                                       Lisp_macro(args_def, content)))

root_env.add_symbol('define-macro',
                    Virtual_table_entry('define-macro',
                                        Lisp_macro(lisp_nil, define)))

# The setter --- mutable.
def setf(symbol, obj, env):
    symbol = eval_lisp_object(symbol, env)
    if symbol.type != Lisp_obj_type.atom or symbol.category != Lisp_atom_category.symbol:
        raise Execution_error('Not setting a symbol')
    obj = eval_body_return_last(obj, env)
    ref = env.find_symbol(symbol.content)
    ref.obj = obj

def setf_bind(body, env):
    symbol = car(body)
    obj = cdr(body)
    setf(symbol, obj, env)

root_env.add_symbol('set',
                    Virtual_table_entry('set',
                                        Lisp_macro(lisp_nil, setf_bind)))

# lambda exp.
def lisp_lambda(args_def, body, env):
    return Lisp_function(args_def, body, env)

def lisp_lambda_bind(body, env):
    args = car(body)
    body = cdr(body)
    return lisp_lambda(args, body, env)

root_env.add_symbol('lambda',
                    Virtual_table_entry('lambda',
                                        Lisp_macro(lisp_nil, lisp_lambda_bind)))

# the py-eval.
def python_eval(obj):
    string = obj.content
    result = eval(string)
    if isinstance(result, Lisp_obj):
        return result
    elif isinstance(result, str):
        return Lisp_atom(Lisp_atom_category.string, result)
    elif isinstance(result, float) or isinstance(result, int):
        return Lisp_atom(Lisp_atom_category.number, result)
    return lisp_nil

_def_simple_function(root_env, 'py-eval', _bind_single_arg_to_py(python_eval))

# process string.
def to_string(obj):
    result = str(obj.content)
    return Lisp_atom(Lisp_atom_category.string, result)

_def_simple_function(root_env, 'to-string', _bind_single_arg_to_py(to_string))

def to_number(obj):
    result = float(obj.content)
    return Lisp_atom(Lisp_atom_category.number, result)

_def_simple_function(root_env, 'to-number', _bind_single_arg_to_py(to_number))
