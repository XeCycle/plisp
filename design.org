Design Notes

* The program flow

  Read the lisp file (in ARGV), execute, and print.  No REPL
  provided.

  Execution:
  1. Setup execute environment.
  2. Load standard libraries.
  3. Load & evaluate libraries in Lisp.
  4. Load & evaluate the file.

  Evaluation:
  1. Construct a stream by the file, or stdin.
  2. Read in one sexp, convert to Lisp object, evaluate.
  3. Repeat step 2, until end of stream.

** The sexp structure

   Sexps are atoms, lists or vectors.  Each can be quoted,
   half-quoted, and eval-in-half-quoted.  No extra information
   allowed.

** The sexp parser

   The algorithm is recursive, but parses char-by-char, from a
   stream.

*** Contexts hierarchy

    All contexts are derived from Context.  Context provides
    `trigger', to check if this context can be triggered by this
    char; and `parse', accepting the stream, parses whatever the
    context think is proper, and return the parsed sexp.  The
    trigger char should be parsed before calling `parse'.  If
    returned sexp is None, it will be ignored.

    Atom_context is a basic abstract context, for representing
    atoms in sexps.  Literal_atom_context and
    Literal_string_context are derived from this context.

    List_context is the context for sexp lists.  Triggered by
    `(', closes itself by `)', returns the list parsed.

    Vector_context is similar to List_context, but triggered by
    `[]'.

    Quote contexts are those related to quotes.  For checking
    backquote-comma pairs, maintain an additional halfquote_depth
    global state.

    Space_context skips spaces, and Comment_context skips
    comments.

*** The parser flow

    The parser reads one sexp at a time, from the stream.
    read_one_sexp is the hi-level process for this, which
    triggers contexts until one parsed sexp is returned (i.e. not
    None).

** The Lisp objects

   Lisp objects are either atoms or conses.  Empty lists are
   automatically converted to `nil' while transforming.

   Atoms can be symbols, numbers, and strings.  Support for
   vectors is under consideration.

   The implementations supports the use of dot for easier cons
   cell creation.

** The executing environment

   Use lexical scoping for functions, dynamic scoping for macros.
   The environment is some symbol tables, which makes a virtual
   "global" environment; symbol tables contain a name => (name =>
   lisp_obj) map, where the latter pair is called a
   Virtual_table_entry.  A root symbol table is set up at program
   start, all other symbol tables are descendants of it.  A
   symbol table is setup with a parent table, and simply refers
   to this parent table's table, until it clones by calling
   fork(), which gets it independent from the parent table.
   
** The evaluation

   eval_lisp_object(obj, env) is the top level function for
   evaluation.  When obj is:

   1. Atom: When not a symbol, return as-is.  Otherwise, find it
      in env and return.
   2. List: (car obj) may be a function, or a macro.  Call the
      corresponding evaluation function, with current env.
      Detailed below.

*** Evaluation of macro

    Macros are first-class, but dynamically scoped.  Macros are
    treated very like functions, except that they don't get the
    arguments evaluated before entering the macro; this allows
    recursive macros.

    Builtin macros may do whatever Python allowed.  Lisp macros
    are evaluated this way:
    1. Clone the current environment.
    2. Setup arguments in the cloned environment.
    3. Evaluate body like normal functions.

*** Evaluation of functions

    Functions are simply closures.  Evaluation:
    1. Evaluate arguments in current environment.
    2. Clone the closure's recorded environment.
    3. Setup arguments.
    4. Evaluate body.
