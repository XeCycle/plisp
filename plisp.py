#!/usr/bin/python2

# Main entry.

import sys

from stream import *
from parse import *
from transform import *
from basicenv import *
from evaluator import *

def main():
    for path in sys.argv[1:]:
        stream = File_stream(path)
        try:
            while True:
                sexp = read_one_sexp(stream)
                obj = transform_to_lisp_obj(sexp)
                result = eval_lisp_object(obj, root_env)
                # Test only
                if result is not None:
                    pretty_print_lisp_obj(result)
        except EOS_exception:
            pass
        except Execution_error as error:
            print error.message
            exit
        except Syntax_error as error:
            print error.message

if __name__ == '__main__':
    main()
